import numpy as np
import pandas as pd
import cryptocompare as cc

# list coins
coin_list = cc.get_coin_list()
coins = sorted(list(coin_list.keys()))

for coin in coins:
    print(coin)
