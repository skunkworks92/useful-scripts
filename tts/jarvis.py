import pyttsx3 as tts

text = "custom.txt"

def jarvis_speak(text):
    engine = tts.init()
    voices = engine.getProperty('voices')
    engine.setProperty('voice', voices[0].id)
    file = open(text, "r", encoding="utf8")
    engine.say(file.read())
    engine.runAndWait()

jarvis_speak(text)