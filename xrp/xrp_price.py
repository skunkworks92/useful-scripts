# import the Shrimpy library for free crypto exchange websockets
import shrimpy

# create the Shrimpy websocket client
client = shrimpy.ShrimpyWsClient()

# define the handler to manage the output stream
def handler(msg):
    # multiple trades can be returned in each message, so take the last one
    ticker = msg['content'][len(msg['content']) - 1]['price']
    print(ticker)

# construct the subscription object
subscribe_data = {
    "type": "subscribe",
    "exchange": "binance",
    "pair": "xrp-usdt",
    "channel": "trade"
}

# connect to the Shrimpy websocket and subscribe
client.connect()
print(client.subscribe(subscribe_data, handler))

# disconnect from the websocket once data collection is complete
client.disconnect()